# gallobiome_haiduong_2017

Description of the gall microbiome by the nematode Meloidogyne graminicola on rice (FEMS MiEco, Masson et al., 2020) Data: lowland rice fields, Hai Duong, Vietnam, 2017 Analyses: diversity, networks and predicted functions of the bacterial community